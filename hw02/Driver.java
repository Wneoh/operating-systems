
import java.util.Scanner;

/**
 * Loads a list of books and then uses multiple threads to search all fields
 * show all matching fields.
 *
 * @author Wei Chin
 * @version 02/11/19
 *
 */
public class Driver {

    public static void main(String[] args) throws Exception {
        BookDatabase db = new BookDatabase("books.txt");  // be sure this file exists!

        while(true){
		System.out.println("-------------------------------------------");
		System.out.print("Please enter a string to perform the search :");
        	Scanner kb = new Scanner(System.in);
        	String searchLine = kb.nextLine();

        	Thread a = new Thread(new SearchThread(searchLine, db, 0)); // search by title
        	Thread b = new Thread(new SearchThread(searchLine, db, 1)); // search by author 
        	Thread c = new Thread(new SearchThread(searchLine, db, 2)); // search by year
        	Thread d = new Thread(new SearchThread(searchLine, db, 3)); // search by ISBN
        	Thread e = new Thread(new SearchThread(searchLine, db, 4)); // search by numofpages
        
        	// start the threads
        	a.start();
        	b.start();
        	c.start();
        	d.start();
        	e.start();

        	// wait for other threads to finish
        	a.join();
        	b.join();
        	c.join();
        	d.join();
        	e.join();
		System.out.println("-------------------------------------------");
	}

    }
}
