
/**
 *
 *  A thread class that has the thread to do different type of search based on the parameter code
 *
 * @author weichin
 * @version 02/11/19
 */
public class SearchThread implements Runnable {

    private String data;
    private int i = 0;
    private int prev = 0;
    private BookDatabase db;
    private int code;

    /**
     * A constructor that accept information for thread to work on
     *
     * @param d as the String that user want to search for
     * @param db as the database pointer
     * @param code as which type of search that each thread use
     */
    public SearchThread(String d, BookDatabase db, int code) {
        data = d;
        this.db = db;
        this.code = code;
    }

    public void run() {
        try {
            SearchRequest request = new SearchRequest(db, data, 0, db.size());
            switch (code) {     // have switch on code to see which type of search it is
                case 0:             // if it is 0 then search by title
                    while (i < db.size()) {
                        db.searchByTitle(request);
                        if (request.foundPos == -1) {       // break the loop if there is no matches found 
                            break;
                        }
                        if (request.foundPos >= 0) {        // else if there is matches print it out and continue searching 
                            System.out.println("Title: " + db.getBook(request.foundPos).toString());
                            prev = request.foundPos;
                        }
                        request.findNext();                         // reset the search by starting the from foundPos
                        i++;
                    }
                    break;
                case 1:     // if it is 1 then search by author
                    while (i < db.size()) {
                        db.searchByAuthor(request);
                        if (request.foundPos == -1) {
                            break;
                        }
                        if (request.foundPos >= 0) {
                            System.out.println("Author: " + db.getBook(request.foundPos).toString());
                            prev = request.foundPos;
                        }
                        request.findNext();
                        i++;
                    }
                    break;
                case 2:     // if it is 2 then search by year
                    while (i < db.size()) {
                        db.searchByYear(request);
                        if (request.foundPos == -1) {
                            break;
                        }
                        if (request.foundPos >= 0) {
                            System.out.println("Year: " + db.getBook(request.foundPos).toString());
                            prev = request.foundPos;
                        }
                        request.findNext();
                        i++;
                    }
                    break;
                case 3:      // if it is 3 then search by ISBN
                    while (i < db.size()) {
                        db.searchByISBN(request);
                        if (request.foundPos == -1) {
                            break;
                        }
                        if (request.foundPos >= 0) {
                            System.out.println("ISBN: " + db.getBook(request.foundPos).toString());
                            prev = request.foundPos;
                        }
                        request.findNext();
                        i++;
                    }
                    break;
                case 4:      // if it is 4 then search by pagenum
                    while (i < db.size()) {
                        db.searchByNumPages(request);
                        if (request.foundPos == -1) {
                            break;
                        }
                        if (request.foundPos >= 0) {
                            System.out.println("NumPages: " + db.getBook(request.foundPos).toString());
                            prev = request.foundPos;
                        }
                        request.findNext();
                        i++;
                    }
                    break;
            }
        } catch (Exception e) {
        }
    }
}
