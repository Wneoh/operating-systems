/**
 * Book class for storing data for a single book.
 *
 * @author  T.Sergeant
 * @version for OS
 *
*/
public class Book
{
  protected String title;
  protected String author;
  protected int year;

  public Book(String newtitle, String newauthor, int newyear)
  {
    title= newtitle;
    author= newauthor;
    year= newyear;
  }

  public String toString()
  {
    return String.format("%-30s %-20s %4d",title,author,year);
  }

  public static void showHeader()
  {
    System.out.print("\n\n----------------------------------------------------------------------------\n");
    System.out.printf("%-30s %-20s %4s\n","Title","Author(s)","Year");
    System.out.print("----------------------------------------------------------------------------\n");
  }
}
