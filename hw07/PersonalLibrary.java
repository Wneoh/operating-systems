/**
 * Personal Library Manager.
 *
 * @author  Terry Sergeant
 * @version for OS
 *
*/

import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class PersonalLibrary
{
  public static final String DATAFILE= "booklist.txt";
  public static final int MAXBOOKS= 5000;
  public static Scanner kb= new Scanner(System.in);

  public static void main(String [] args) throws IOException
  {
    int choice;             // user's selection from the menu
    Book [] books= new Book[MAXBOOKS];
    int numberOfEntries;

    numberOfEntries= loadBooks(books);
    do {
      choice= menuChoice();
      switch (choice) {
        case 1: numberOfEntries= addBook(books,numberOfEntries); break;
        case 2: updateBook(books,numberOfEntries); break;
        case 3: numberOfEntries= deleteBook(books,numberOfEntries); break;
        case 4: displayAll(books,numberOfEntries); break;
        case 5: searchByAuthor(books,numberOfEntries); break;
        case 6: searchByTitle(books,numberOfEntries); break;
      }
    } while (choice!=9);

    saveBooks(books,numberOfEntries);

    System.out.println("\nTHE END");
  }


  /**
   * Deletes a book specified by the user.
   *
   * @param books array of book titles, authors, copyright dates
   * @param n the actual number of books currently in the array
   * @return the new number of book entries
   *
   * <pre>
   * We will be "lazy" and have the user select a book by number so we need
   * to modify the display book code to list numbers beside each book.
   * </pre>
  */
  public static int deleteBook(Book [] books, int n)
  {
    int pos= getPos("removed",n);
    books[pos]= books[n-1];
    System.out.println("\n\n");
    return n-1;
  }



  /**
   * Updates a book specified by the user.
   *
   * @param books array of book titles, authors, copyright dates
   * @param n the actual number of books currently in the array
   *
   * <pre>
   * We will be "lazy" and have the user select a book by number so we need
   * to modify the display book code to list numbers beside each book.
   * </pre>
  */
  public static void updateBook(Book [] books, int n)
  {
    int pos= getPos("updated",n);
    books[pos]= books[n-1];
    System.out.println("You are updating the following book:\n\n");
    Book.showHeader();
    System.out.println(books[pos]);
    books[pos]= enterBookInfo();
    System.out.println("\n\n");
  }



  /**
   * Inserts a new book w/ input coming from the user.
   *
   * @param books array of book titles, authors, copyright dates
   * @param n the actual number of books currently in the array
   * @return the new number of book entries
   *
   * <pre>
   * We could enhance this so that it keeps the books in alphabetical
   * order!   But we'll keep it simple for now.
   * </pre>
  */
  public static int addBook(Book [] books, int n)
  {
    books[n]= enterBookInfo();
    return n+1;
  }


  /**
   * Searches (case insenstive) for a book by title (allowing partial
   * matches).
   *
   * @param books array of book titles, authors, copyright dates
   * @param n the actual number of books currently in the array
  */
  public static void searchByTitle(Book [] books, int n)
  {
    int i,matchCount;
    String title;

    System.out.print("\n\nEnter the title you want to search for: ");
    title= kb.nextLine().toLowerCase();

    displayHeader();
    matchCount= 0;
    for (i=0; i<n; i++)
      if (books[i].title.toLowerCase().indexOf(title)>=0) {
        System.out.printf("[%4d] %s\n",i,books[i]);
        matchCount++;
      }

    if (matchCount==0)
      System.out.println("No matching titles found");
  }



  /**
   * Searches (case insenstive) for a book by author (allowing partial
   * matches).
   *
   * @param books array of book titles, authors, copyright dates
   * @param n the actual number of books currently in the array
  */
  public static void searchByAuthor(Book [] books, int n)
  {
    int i,matchCount;
    String author;

    System.out.print("\n\nEnter the author you want to search for: ");
    author= kb.nextLine().toLowerCase();

    displayHeader();
    matchCount= 0;
    for (i=0; i<n; i++)
      if (books[i].author.toLowerCase().indexOf(author)>=0) {
        System.out.printf("[%4d] %s\n",i,books[i]);
        matchCount++;
      }

    if (matchCount==0)
      System.out.println("No matching authors found");
  }




  /**
   * Load books from the data file into the arrays
   *
   * @param books array of book titles, authors, copyright dates
   * @return the actual number of books loaded into the arrays
  */
  public static int loadBooks(Book [] books) throws IOException
  {
    Scanner f= new Scanner(new File(DATAFILE));
    int i= 0;

    while (f.hasNextLine()) {
      books[i]= new Book(f.nextLine(),f.nextLine(),f.nextInt());
      f.nextLine();
      i++;
    }

    f.close();

    return i;
  }


  /**
   * Save books from the array into the data file
   *
   * @param books array of book titles, authors, copyright dates
   * @return the actual number of books loaded into the arrays
  */
  public static void saveBooks(Book [] books, int n) throws IOException
  {
    PrintStream out= new PrintStream(new File(DATAFILE));
    int i;

    for (i=0; i<n; i++) {
      out.println(books[i].title);
      out.println(books[i].author);
      out.println(books[i].year);
    }
    out.close();
  }


  /**
   * Displays header for a list of books.
  */
  public static void displayHeader()
  {
    System.out.println("------------------------------------------------------------------------");
    System.out.printf("[%-4s] %-30s %-20s %4s\n","Pos","Title","Author","Year");
    System.out.println("------------------------------------------------------------------------");
  }


  /**
   * Displays all book information.
   *
   * @param books array of book titles, authors, copyright dates
   * @param n the actual number of books currently in the array
  */
  public static void displayAll(Book [] books, int n)
  {
    int i;
    displayHeader();
    for (i=0; i<n; i++)
      System.out.printf("[%4d] %s\n",i,books[i]);
  }




  /**
   * Displays menu and get's user's selection
   *
   * @return the user's menu selection
  */
  public static int menuChoice()
  {
    Scanner kb= new Scanner(System.in);
    int choice;   // user's selection

    System.out.println("\n\n");
    System.out.print("------------------------------------\n");
    System.out.print("[1] Add a Book\n");
    System.out.print("[2] Update a Book\n");
    System.out.print("[3] Delete a Book\n");
    System.out.print("[4] List All Books\n");
    System.out.print("[5] Search by Author\n");
    System.out.print("[6] Search by Title\n");
    System.out.print("[9] Exit Program\n");
    System.out.print("------------------------------------\n");
    do {
      System.out.print("Your choice: ");
      choice= kb.nextInt();
    } while (choice < 1 || (choice > 6 && choice != 9));

    return choice;
  }

  /**
   * Allows user to select a book by its number.
   *
   * @return number of book selected
  */
  public static int getPos(String action, int n)
  {
    int pos;             // position at which numberToRemove is found

    do {
      System.out.print("Enter number of book to be "+action+": ");
      pos= kb.nextInt();
    } while (pos<0 || pos>=n);
    kb.nextLine();

    return pos;
  }


  /**
   * Allows user to enter title/author/year for a book.
   *
   * @return reference to the new book object0
  */
  public static Book enterBookInfo()
  {
    String title, author;
    int year;

    System.out.println();
    System.out.println();
    System.out.print("Enter a title: ");
    title= kb.nextLine();
    System.out.print("Enter an author: ");
    author= kb.nextLine();
    System.out.print("Enter a year: ");
    year= kb.nextInt();
    kb.nextLine();
    return new Book(title,author,year);
  }
}



