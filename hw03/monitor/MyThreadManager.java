
/**
 * Keeps track of which threads are busy and which ones can be assigned work.
 *
 * @author Wei Chin
 * @version 02/16/19
 *
 */
import java.io.PrintStream;
import java.net.Socket;

public class MyThreadManager {

    private boolean[] busy;   // which threads are busy
    private Thread[] workers; // array of threads

    /**
     * We default to 5 threads in server pool.
     */
    public MyThreadManager() {
        this(5);
    }

    /**
     * Initially none of the threads is busy.
     *
     * @param n number of threads allowed in server pool
     */
    public MyThreadManager(int n) {
        workers = new Thread[n];
        busy = new boolean[n];
        for (int i = 0; i < n; i++) {
            busy[i] = false;
        }
    }

    /**
     * Find the first non-busy thread and return it.
     *
     * @return position in array of a non-busy thread
     */
    private int nextOpen() {
        for (int i = 0; i < workers.length; i++) {
            if (!busy[i] || workers[i].getState() == Thread.State.TERMINATED) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Create a new server thread to handle incoming request.
     */
    public void assign(Socket con, BookDatabase db) throws Exception {
        int pos;
        PrintStream output = new PrintStream(con.getOutputStream());

        pos = nextOpen();	// look for an unused slot
        if (pos < 0) {
            output.println("Server is currently busy. Please try to connect later");
            System.out.println("Server is busy now");
            output.flush();
            output.close();
            con.close();	// close the connection      

        } else {
            busy[pos] = true;
            workers[pos] = new Thread(new ServerThread(con, db));
            workers[pos].start();
            System.out.println("Assigned connection in slot " + pos + " to " + workers[pos].getName());
        }
    }
}
